package com.fileupload.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fileupload.services.ImageUploader;

@RestController
@RequestMapping("/api/v1/s3")
public class S3Controller
{
	@Autowired
	private ImageUploader uploader;

	@PostMapping("/")
	private ResponseEntity<?> uploadImag(@RequestParam MultipartFile file)
	{
		return ResponseEntity.ok(uploader.uploadImage(file));

	}

	@GetMapping("/")
	public ResponseEntity<?> getAllFiles()
	{
		return ResponseEntity.ok(uploader.allFiles());
	}

	@GetMapping("/{fileName}")
	public ResponseEntity<?> getFileByName(@PathVariable("fileName") String fileName)
	{
		return ResponseEntity.ok(uploader.getFileByName(fileName));
	}

}
