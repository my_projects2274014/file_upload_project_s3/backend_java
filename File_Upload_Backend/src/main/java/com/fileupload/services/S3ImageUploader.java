package com.fileupload.services;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fileupload.exception.ImageUploaderException;

@Service
public class S3ImageUploader implements ImageUploader
{

	@Autowired
	private AmazonS3 client;

	@Value("${app.s3.bucket}")
	private String bucket;

	@Override
	public String uploadImage(MultipartFile image)
	{
		if (Objects.isNull(image))
		{
			throw new ImageUploaderException("Image is null");
		}
		String originalFilename = image.getOriginalFilename();
		String fileName = UUID.randomUUID() + originalFilename.substring(originalFilename.lastIndexOf("."));

		ObjectMetadata metaData = new ObjectMetadata();
		metaData.setContentLength(image.getSize());

		try
		{
			client.putObject(new PutObjectRequest(bucket, fileName, image.getInputStream(),
					metaData));
			return this.preSignedUrl(fileName);
		} catch (IOException e)
		{
			throw new ImageUploaderException("Error in the file upload " + e.getMessage());
		}
	}

	@Override
	public List<Map<String, String>> allFiles()
	{
		ListObjectsV2Request listObjectsV2Request = new ListObjectsV2Request().withBucketName(bucket);
		ListObjectsV2Result listObjectsV2 = client.listObjectsV2(listObjectsV2Request);
		List<S3ObjectSummary> objectSummaries = listObjectsV2.getObjectSummaries();
		List<Map<String, String>> listFileUrls = objectSummaries.stream()
				.map(item -> Map.of(item.getKey(), this.preSignedUrl(item.getKey())))
				.collect(Collectors.toList());

		return listFileUrls;
	}

	@Override
	public String preSignedUrl(String fileName)
	{
		Date expirationDate = new Date();
		long time = expirationDate.getTime();
		int hr = 2;
		time += hr * 60 * 60 * 1000;
		expirationDate.setTime(time);

		GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket, fileName)
				.withMethod(HttpMethod.GET)
				.withExpiration(expirationDate);

		URL url = client.generatePresignedUrl(generatePresignedUrlRequest);
		return url.toString();
	}

	@Override
	public String getFileByName(String fileName)
	{
		S3Object object = client.getObject(bucket, fileName);
		String url = this.preSignedUrl(object.getKey());
		return url;
	}

}
