package com.fileupload.services;

import org.springframework.web.multipart.MultipartFile;

import java.util.*;

public interface ImageUploader
{
	String uploadImage(MultipartFile image);
	
	List<Map<String, String>> allFiles();
	
	String preSignedUrl(String fileName);
	
	String getFileByName(String fileName);
}
