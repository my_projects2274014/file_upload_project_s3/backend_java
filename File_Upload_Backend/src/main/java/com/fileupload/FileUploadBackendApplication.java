package com.fileupload;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileUploadBackendApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(FileUploadBackendApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Started the spring boot project..!!");
		
	}

}
