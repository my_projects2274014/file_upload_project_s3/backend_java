package com.fileupload.exception;

public class CustomResponse
{
	private String message;
	private boolean success;

	public CustomResponse()
	{
	}

	public CustomResponse(String message, boolean success)
	{
		this.message = message;
		this.success = success;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public static Builder builder()
	{
		return new Builder();
	}

	@Override
	public String toString()
	{
		return "CustomResponse [message=" + message + ", success=" + success + "]";
	}

}

class Builder
{
	private String message;
	private boolean success;

	public Builder()
	{
	}

	public Builder message(String message)
	{
		this.message = message;
		return this;
	}

	public Builder success(boolean success)
	{
		this.success = success;
		return this;
	}

	public CustomResponse build()
	{
		return new CustomResponse(this.message, this.success);
	}

}
