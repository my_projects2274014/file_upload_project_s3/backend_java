package com.fileupload.exception;

public class ImageUploaderException extends RuntimeException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ImageUploaderException(String message)
	{
		super(message);
	}
}
