package com.fileupload.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler
{
	@ExceptionHandler(ImageUploaderException.class)
	public ResponseEntity<?> handleImageUploaderException(ImageUploaderException imageUploaderException)
	{
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(CustomResponse.builder()
						.message(imageUploaderException.getMessage())
						.success(false)
						.build());

	}
}
